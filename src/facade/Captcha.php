<?php

namespace YangPeng\WebManCaptcha\Facade;

/**
 * 图片验证码
 * @method static string create() 返回验证码图片和验证码串
 * @method static string check($key, $value) 校验验证码
 */
class Captcha
{
    public static $instance = null;

    public static function instance()
    {
        if (!static::$instance) {
            static::$instance = new \YangPeng\WebManCaptcha\Captcha();
        }
        return static::$instance;
    }

    public static function __callStatic($method, $arguments)
    {
        return static::instance()->{$method}(... $arguments);
    }
}